import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        try {
            FileInputStream fstream =
                    new FileInputStream("src/main/resources/greeneggsandham.txt");
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            String strLine;
            HashMap<String, Integer> map = new HashMap<String, Integer>();

            while ((strLine = br.readLine()) != null){
                String[] list = strLine.toLowerCase().split(" ");
                for (int i = 0; i < list.length; i++) {
                    if (list[i].equals("")) {
                        continue;
                    }
                    if(map.containsKey(list[i])) {
                        map.put(list[i], map.get(list[i]) + 1);
                    } else {
                        map.put(list[i], 1);
                    }
                }
            }

            List<String> list = new ArrayList<String>(map.keySet());

            Collections.sort(list);

            FileWriter writer = new FileWriter("src/main/resources/output.txt", false);

            for (String s : list) {
                String answer = s + " " + map.get(s) + "\n";
                writer.write(answer);
                writer.flush();
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
